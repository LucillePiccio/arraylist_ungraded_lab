package listactivity;

import java.util.ArrayList;

public class ListPractice{
    public static void main (String[] args){
        //tester array
        String[] fruits = {"Cherry", "AVOCADOE", "Orange","APPLE","BANANA"};
    
        //testing isUpperCase
        //boolean isUpperCase = isUpperCase(fruits);
        //System.out.println(isUpperCase);

        //testing countUpperCase
        //int upperCase = countUpperCase(fruits);
        //System.out.println(upperCase);

        //testing getUpperCase
        String[] getUpperCaseArray = getUpperCase(fruits);
        for(int i = 0; i < getUpperCaseArray.length; i++){
            System.out.println(fruits[i]);
        }

    //part2
    /* 
    ArrayList<String> words = new ArrayList<String>();
    System.out.println(words.size());

    ArrayList<String> stillEmpty = new ArrayList<String>(50);
    System.out.println(stillEmpty.size());

    //using foreach method to add content of fruits array
    for(String f: fruits){
        stillEmpty.add(f);
    }
    System.out.println(stillEmpty);
    System.out.println("the size of array stillEmpty is now " + stillEmpty.size());

    //now doing the same exercise as Part1 but with ArrayList
    int arrListCounter = 0;
    for(String i: fruits){
        if(i.matches("^[A-Z]+$")){
            arrListCounter++;
        }

    ArrayList<String> newArr = new ArrayList<>(arrListCounter);
*/
    }
    //1
    public static int countUpperCase(String[] str){
        int upperCaseNum = 0;
            if(isUpperCase(str)){
                upperCaseNum++;
        }
        return upperCaseNum;
    }

    //helper method for countUpperCase
    public static boolean isUpperCase(String[] str){
        boolean isUpper = false;
        for(int i = 0; i< str.length; i++){
            if(str[i].matches("^[A-Z]+$")){
                isUpper = true;
            }
        }
        return isUpper;
    }

    //2
    //counts how many words are upperCase only
    public static String[] getUpperCase(String[] strings){
        int counter1 = 0;
        for(int i = 0; i < strings.length; i++){
            if(strings[i].matches("^[A-Z]+$")){
                counter1++;
            }
        }

        //creating a new array, where its length is the number of counter returned
        String[] newArr = new String[counter1];
        int counter2 = 0;
        //instead of the fruits array, the length will be the counter we had 
        for(int i = 0; i < counter1; i++){
            //issue with index array out of bounds
            if(strings[i].matches("^[A-Z]+$")){
                //using counter2 as index to avoid being OOB
                newArr[counter2] = strings[i];
                counter2++;
            }
        }
        return newArr;
    }
}